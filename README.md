# Good migrations for knex

Migrate anything and store the migration status in your database,
using knex as the database client.

```javascript
import createMigrator from 'good-migrations-knex';
import knex from 'knex';

const db = knex({
	client: 'sqlite3',
	connection: {
		filename: ':memory:'
	}
});

const migrate = await createMigrator(db, 'baking a cake');

// You migrations don't have to be sql-related.
await migrate([
	()=>cake.addFlour(),
	()=>cake.addSugar(),
	()=>cake.addEggs(),
	()=>oven.add(cake)
]);

```

## Dependencies
 - good-migrations: ^1.0.1

<a name="module_gmKnex"></a>

## gmKnex

* [gmKnex](#module_gmKnex)
    * [~migratorFactory](#module_gmKnex..migratorFactory) ⇒ <code>Migrator</code>
    * [~Migrator](#module_gmKnex..Migrator) : <code>function</code>

<a name="module_gmKnex..migratorFactory"></a>

### gmKnex~migratorFactory ⇒ <code>Migrator</code>
Create a migrator function

**Kind**: inner property of [<code>gmKnex</code>](#module_gmKnex)
**Returns**: <code>Migrator</code> - Migrator function

| Param | Type | Description |
| --- | --- | --- |
| knex | <code>Knex</code> | Knex instance |
| key | <code>string</code> | Key to identify this set of migrations |

<a name="module_gmKnex..Migrator"></a>

### gmKnex~Migrator : <code>function</code>
**Kind**: inner typedef of [<code>gmKnex</code>](#module_gmKnex)

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| migrations | <code>Array.&lt;function()&gt;</code> |  | Array of migration functions to run |
| [startIndex] | <code>Number</code> | <code>0</code> | The version number of the first migration |

