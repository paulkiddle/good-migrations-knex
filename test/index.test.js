import { jest } from '@jest/globals';
import knex from 'knex';
import makeMigrator from '../src/index.js';

const db = knex({
	client: 'sqlite3',
	connection: {
		filename: ':memory:'
	}
});


test('Throws if no key', ()=>{
	expect(makeMigrator(db)).rejects.toBeInstanceOf(TypeError);
	expect(makeMigrator(db, 'key')).resolves.toBeInstanceOf(Function);
})

const migrate = await makeMigrator(db, 'key');

test('Runs migrations once, in order', async ()=>{
	const migrations = [
		jest.fn()
	];

	await migrate(migrations);

	expect(migrations[0]).toHaveBeenCalled();

	await migrate(migrations);

	expect(migrations[0]).toHaveBeenCalledTimes(1);
});

test('Migrations run from current version', async ()=>{
	const migrations = [
		jest.fn(),
		jest.fn()
	];

	await migrate(migrations);

	expect(migrations[0]).not.toHaveBeenCalled();
	expect(migrations[1]).toHaveBeenCalled();
});

test('Specify start version', async ()=> {
	const migrations = [
		jest.fn(),
		jest.fn()
	];

	await migrate(migrations, 1);

	expect(migrations[0]).not.toHaveBeenCalled();
	expect(migrations[1]).toHaveBeenCalled();
});

test('Throw if start version is too high', async ()=>{
	await expect(migrate([], 3)).resolves.not.toThrow();
	await expect(migrate([], 4)).rejects.toThrow();
});

test('Stops on error', async () => {
	const migrations = [
		jest.fn(()=>{ throw 'Error';}),
		jest.fn()
	];

	await expect(migrate(migrations, 3)).rejects.toBe('Error');
	expect(migrations[1]).not.toHaveBeenCalled();

	const migrations2 = [
		jest.fn()
	];

	await migrate(migrations2, 3);

	expect(migrations2[0]).toHaveBeenCalled();
});
