# Good migrations for knex

Migrate anything and store the migration status in your database,
using knex as the database client.

```javascript
import createMigrator from 'good-migrations-knex';
import knex from 'knex';

const db = knex({
	client: 'sqlite3',
	connection: {
		filename: ':memory:'
	}
});

const migrate = await createMigrator(db, 'baking a cake');

// You migrations don't have to be sql-related.
await migrate([
	()=>cake.addFlour(),
	()=>cake.addSugar(),
	()=>cake.addEggs(),
	()=>oven.add(cake)
]);

```
