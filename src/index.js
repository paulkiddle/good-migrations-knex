import migrate from 'good-migrations';

/**
 * @module gmKnex
 */

/**
 * @callback Migrator
 * Runs a set of migrations
 * @async
 * @param {Function[]} migrations Array of migration functions to run
 * @param {Number} [startIndex=0] The version number of the first migration
 */

/**
 * Create a migrator function
 * @name migratorFactory
 * @param {Knex} knex Knex instance
 * @param {string} key Key to identify this set of migrations
 * @return {Migrator} Migrator function
 */
export default async function migrateKnex(knex, key) {
	if(!key) {
		throw new TypeError('Must pass a key as second argument');
	}
	const storage = {
		async getVersion(){
			const rows = await knex.select('version').from('good_migrations').where('key', '=', key);
			return rows && rows[0] && rows[0].version;
		},
		async setVersion(version){
			const v = await this.getVersion();
			if(v == null) {
				await knex.insert({ key, version }).into('good_migrations');
			} else {
				await knex('good_migrations').update({ version }).where('key', '=', key);
			}
		},
	};

	if(!await knex.schema.hasTable('good_migrations')) {
		await knex.schema.createTable('good_migrations', table => {
			table.string('key').primary();
			table.integer('version');
		});
	}

	return (migrations, startIndex) => migrate(storage, migrations, startIndex);
}
